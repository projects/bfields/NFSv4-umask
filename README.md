This draft defines a new NFSv4 attribute to communicate the un-umasked
mode used when creating a new file.

http://www.ietf.org/html.charters/nfsv4-charter.html
http://www.ietf.org/mail-archive/web/nfsv4/index.html

NOTE: The drafts contained here may change after it is posted to the IETF site. The ITEF site is always the definitive authority on content.

To compile the XML files into text files:

1. Install xml2rfc 
   http://xml.resource.org/

   These instructions have been tested with version 2

2. Build

   > make

