<!-- Copyright (C) The IETF Trust (2014) -->
<!-- Copyright (C) The Internet Society (2014) -->

<section anchor="sec:solution" title="mode_umask Attribute">

  <t>
    &lt;CODE BEGINS&gt;
  </t>
  <figure>
    <artwork>
///   struct mode_umask4 {
///     mode4  mu_mode;
///     mode4  mu_umask;
///   };
///
///   %/*
///   % * New For UMASK
///   % */
///   const FATTR4_MODE_UMASK         = 81;
    </artwork>
  </figure>
  <t>
    &lt;CODE ENDS&gt;
  </t>

  <texttable anchor="tbl:rec_attr">
    <ttcol align="left">Name</ttcol>
    <ttcol align="left">Id</ttcol>
    <ttcol align="left">Data Type</ttcol>
    <ttcol align="left">Acc</ttcol>
    <ttcol align="left">Defined in</ttcol>
    <c>mode_umask</c>    <c>81</c> <c>mode_umask4</c>           <c>  W</c> <c><xref target="sec:solution" /></c>
  </texttable>

  <t>
    The NFSv4.2 mode_umask attribute is based on the umask and on the
    mode bits specified at open time, which together determine the mode
    of a newly created UNIX file.  Only the nine low-order mode4 bits of
    mu_umask are defined.  A server MUST return NFS4ERR_INVAL if bits
    other than those nine are set.
  </t>

  <t>
    The mode_umask attribute is only meaningful for operations that
    create objects (CREATE and OPEN); in other operations that take
    fattr4 arguments, the server MUST reject it with NFS4ERR_INVAL.
  </t>

  <t>
    The server MUST return NFS4ERR_INVAL if the client attempts to set
    both mode and mode_umask in the same operation.
  </t>

  <t>
    When the server supports the mode_umask attribute, a client creating
    a file should use mode_umask in place of mode, with mu_mode set to
    the unmodified mode provided by the user, and mu_umask set to the
    umask of the requesting process.
  </t>

  <t>
    The server then uses mode_umask as follows:
    <list style="symbols">
      <t>On a server that supports ACL attributes, if an object inherits
      any ACEs from its parent directory, mu_mode SHOULD be used,
      and mu_umask ignored.</t>
      <t>Otherwise, mu_umask MUST be used to limit the mode: all bits
      in the mode MUST be turned off which are set in the umask; the
      mode assigned to the new object becomes (mu_mode &amp; ~mu_umask)
      instead.  </t>
    </list>
  </t>

</section>
